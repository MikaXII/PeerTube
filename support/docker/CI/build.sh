#!/bin/bash
TAG=$1
ARTIFACTS_DIR=${PWD}/artifact
echo "Remove artifact"
rm -rf artifact/*
echo "Docker build peertube-build"
docker build -f Dockerfile.build -t peertube-build .
echo "Docker run peertube-build"
docker run --rm -it -v `pwd`/artifact:/artifact peertube-build
echo "Docker build peertube"
docker build -t mikaxii/peertube:$TAG .
echo "Docker push peertube"
docker push mikaxii/peertube:$TAG
